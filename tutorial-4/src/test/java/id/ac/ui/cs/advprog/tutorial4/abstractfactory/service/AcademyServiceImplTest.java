package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    private AcademyRepository academyRepository;
    private AcademyServiceImpl academyServiceImpl;

    @Mock
    private AcademyRepository academyRepositoryMock;

    @InjectMocks
    private AcademyServiceImpl academyServiceImplMock;

    @BeforeEach
    public void setUp() throws Exception {
        this.academyRepository = new AcademyRepository();
        this.academyServiceImpl= new AcademyServiceImpl(this.academyRepository);
    }


    @Test
    public void testProduceKnightAndGetKnight() {
        this.academyServiceImpl.produceKnight("Lordran", "majestic");
        assertThat(academyServiceImpl.getKnight() instanceof MajesticKnight).isTrue();
    }


    @Test
    public void whenGetKnightAcademiesIsCalledItShouldCallAcademyRepositoryGetKnightAcademies() {
        this.academyServiceImplMock.getKnightAcademies();
        verify(this.academyRepositoryMock, times(1)).getKnightAcademies();
    }

    @Test
    public void testGetKnightAcademies() {
        assertThat(this.academyRepository.getKnightAcademies()).isEqualTo(this.academyServiceImpl.getKnightAcademies());
    }

}
