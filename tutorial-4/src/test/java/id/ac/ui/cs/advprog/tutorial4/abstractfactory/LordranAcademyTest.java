package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        this.lordranAcademy = new LordranAcademy();
        this.majesticKnight = this.lordranAcademy.getKnight("majestic");
        this.metalClusterKnight = this.lordranAcademy.getKnight("metal cluster");
        this.syntheticKnight= this.lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(this.majesticKnight instanceof MajesticKnight);
        assertTrue(this.metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(this.syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Knight", this.majesticKnight.getName());
        assertEquals("Metal Cluster Knight", this.metalClusterKnight.getName());
        assertEquals("Synthetic Knight", this.syntheticKnight.getName());
    }

    @Test
    public void checkAcademyName() {
        assertEquals("Lordran", this.lordranAcademy.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        assertEquals("Knight: Majestic Knight", this.majesticKnight.getDescription());
        assertEquals("Knight: Metal Cluster Knight", this.metalClusterKnight.getDescription());
        assertEquals("Knight: Synthetic Knight", this.syntheticKnight.getDescription());
    }
}
