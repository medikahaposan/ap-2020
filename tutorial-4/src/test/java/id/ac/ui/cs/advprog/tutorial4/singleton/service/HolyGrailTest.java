package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyWish holyWish;

    @InjectMocks
    private HolyGrail holyGrail;

    @Test
    public void whenMakeAWishIsCalledItShouldCallHolyWishSetWish() {
        this.holyGrail.makeAWish("Aku mau selesai");
        verify(this.holyWish, times(1)).setWish("Aku mau selesai");
    }

    @Test
    public void whenGetHolyWishIsCalledItShouldReturnHolyWish() {
        HolyWish holyWish = this.holyGrail.getHolyWish();
        assertThat(holyWish).isEqualTo(this.holyWish);
    }

}
