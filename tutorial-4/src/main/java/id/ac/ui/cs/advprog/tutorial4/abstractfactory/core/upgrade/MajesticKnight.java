package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
        this.description = "Knight: Majestic Knight";
        this.name = "Majestic Knight";
    }

    @Override
    public void prepare() {
        this.weapon = this.armory.craftWeapon();
        this.armor = this.armory.craftArmor();
    }
}
