package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
        this.description = "Knight: Synthetic Knight";
        this.name = "Synthetic Knight";
    }

    @Override
    public void prepare() {
        this.weapon = this.armory.craftWeapon();
        this.skill = this.armory.learnSkill();
    }
}
