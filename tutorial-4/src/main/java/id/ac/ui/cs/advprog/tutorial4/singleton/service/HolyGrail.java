package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.springframework.stereotype.Service;

@Service
public class HolyGrail {
    private HolyWish holyWish;

    public HolyGrail(){
        this.holyWish = HolyWish.getInstance();
    }

    public void makeAWish(String wish) {
        this.holyWish.setWish(wish);
    }

    public HolyWish getHolyWish() {
        return this.holyWish;
    }
}
