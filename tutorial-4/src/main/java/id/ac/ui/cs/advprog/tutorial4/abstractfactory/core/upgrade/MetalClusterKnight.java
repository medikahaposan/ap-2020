package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
        this.description = "Knight: Metal Cluster Knight";
        this.name = "Metal Cluster Knight";
    }

    @Override
    public void prepare() {
        this.armor = this.armory.craftArmor();
        this.skill = this.armory.learnSkill();
    }
}
