package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public String countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return powerClassifier(rawAge*2000);
        } else if (rawAge <50) {
            return powerClassifier(rawAge*2250);
        } else {
            return powerClassifier(rawAge*5000);
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }

    private String powerClassifier(int power){
        //asumsi tidak inklusif di batas akhir
        if (power >= 0 && power < 20000){
            return power + " C class";
        } else if (power >= 20000 && power < 100000){
            return power + " B class";
        } else if (power >= 100000){
            return power + " A class";
        } else {
            return "Cannot Classified";
        }
    }
}
