package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    AttackBehavior attackBehavior;
    DefenseBehavior defenseBehavior;
    public KnightAdventurer(){
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }
    public String getAlias(){
        return "Knight";
    }
}
