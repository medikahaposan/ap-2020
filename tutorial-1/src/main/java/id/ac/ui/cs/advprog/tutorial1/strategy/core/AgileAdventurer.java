package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        AttackBehavior attackBehavior;
        DefenseBehavior defenseBehavior;
        public AgileAdventurer(){
            this.setAttackBehavior(new AttackWithGun());
            this.setDefenseBehavior(new DefendWithBarrier());
        }
        public String getAlias(){
            return "Agile";
        }
}
