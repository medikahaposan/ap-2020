package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
        }

        @Override
        public void update(){
                String guildType = this.guild.getQuestType();
                if (guildType   .equalsIgnoreCase("D") || guildType.equalsIgnoreCase("E")){
                        this.getQuests().add(guild.getQuest());
                }
        }
}
