package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                this.guild = guild;
        }

        @Override
        public void update(){
                String guildType = this.guild.getQuestType();
                if (guildType.equalsIgnoreCase("D") || guildType.equalsIgnoreCase("R") || guildType.equalsIgnoreCase("E")){
                        this.getQuests().add(guild.getQuest());
                }
        }
}
