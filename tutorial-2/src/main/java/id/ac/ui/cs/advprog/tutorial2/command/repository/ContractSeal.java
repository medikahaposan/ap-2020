package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private boolean isCanUndo = true;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        spells.get(spellName).cast();
        this.latestSpell = spells.get(spellName);
        this.isCanUndo = true;
    }

    public void undoSpell() {
        if (this.latestSpell == null || !isCanUndo){}
        else {
            latestSpell.undo();
            this.isCanUndo = false;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
