package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spell = new ArrayList<>();

    public ChainSpell(ArrayList<Spell> spell){
        this.spell = spell;
    }

    @Override
    public void cast() {
        if (spell.size() > 0) {
            for (int i = 0; i < this.spell.size(); i++){
                this.spell.get(i).cast();
            }
        }
    }

    @Override
    public void undo() {
        if (spell.size() > 0) {
            for (int i = this.spell.size()-1; i >= 0 ; i--){
                this.spell.get(i).cast();
            }
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
